import { INestApplication } from '@nestjs/common';
import { CacheSpecService } from './fixtures/cache.service';
import { createApp } from '../src/';

describe('cache spec', () => {
  let app: INestApplication;
  let cacheSpecService: CacheSpecService;
  beforeAll(async () => {

    app = await createApp({ providers: [CacheSpecService] });
    await app.init();

    cacheSpecService = await app.get(CacheSpecService);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('local cache', () => {

    it('should return the same value', async () => {
      const result1 = await cacheSpecService.addRandomNumber(10);
      const result2 = await cacheSpecService.addRandomNumber(10);
      expect(result1).toEqual(result2);
    });

    it('should return different value', async () => {
      const result1 = await cacheSpecService.addRandomNumber(1);
      const result2 = await cacheSpecService.addRandomNumber(3);
      expect(result1 !== result2).toEqual(true);
    });
  });

});
