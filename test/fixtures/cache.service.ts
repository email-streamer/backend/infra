import { randomInt } from 'crypto';
import { Injectable } from '@nestjs/common';
import { Cache } from '../../src/';

@Injectable()
export class CacheSpecService {
  @Cache({ key: 'test_key', ttl: 5 })
  async addRandomNumber(initialNumber: number): Promise<number> {
    const randomNumber = randomInt(999999);

    return initialNumber + randomNumber;
  }
}
