import { INestApplication } from '@nestjs/common';
import { createApp } from '../src';
import { SYSTEM_LOGGER_TOKEN } from '../src/logger/constants';

describe('logger spec', () => {
  let app: INestApplication;

  beforeAll(async () => {
    app = await createApp({});
    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it('should be successfully called', () => {
    const systemLogger = app.get(SYSTEM_LOGGER_TOKEN);
    const stdoutSpy = jest.spyOn(systemLogger, 'log');
    systemLogger.log('test info');
    expect(stdoutSpy).toBeCalledWith('test info');
  });

});
