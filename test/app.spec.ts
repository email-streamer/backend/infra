import { HttpStatus, INestApplication } from '@nestjs/common';
import * as supertest from 'supertest';
import { createApp } from '../src';
import { HealthCheckerController } from '../src/app/health-checker';

describe('app spec', () => {
  let app: INestApplication;
  let request: supertest.SuperTest<supertest.Test>;

  beforeAll(async () => {
    app = await createApp({ controllers: [HealthCheckerController] });
    await app.init();
    request = supertest(app.getHttpServer());
  });

  afterAll(async () => {
    await app.close();
  });

  describe('health', () => {

    it('should return success on health check', async () => {
      await request.get('/health').send().expect(HttpStatus.OK);
    });

  });

  describe('swagger', () => {

    it('should return swagger document', async () => {
      await request.get('/swagger').send().expect(HttpStatus.OK);
    });

  });

});
