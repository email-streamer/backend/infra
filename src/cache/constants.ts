export const CACHE_MANAGER_TOKEN = Symbol('MAIN_CACHE_MANAGER');
export const LOCAL_CACHE_INJECTION_TOKEN = Symbol('LOCAL_CHACHE');
export const CACHE_KEY_PREFIX = 'cache:methods:';
export const CACHE_INJECTION_PROP = '$cache';
export const CACHE_MEMORY_STORE = 'memory';
export const CACHE_REDIS_STORE = 'redis';
