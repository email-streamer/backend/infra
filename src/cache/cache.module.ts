import {
  DynamicModule,
  Global,
  Module,
} from '@nestjs/common';
import * as cacheManager from 'cache-manager';
import redisStore from 'cache-manager-ioredis';
import { CACHE_MANAGER_TOKEN } from './constants';
import { CacheConfig } from './types';
import { defaultCacheConfig } from '../config/cache.config';

@Module({})
@Global()
export class CacheModule {
  static forRoot(configKey?: string): DynamicModule {
    const module: DynamicModule = {
      module: CacheModule,
    };

    module.providers = [
      {
        provide: CACHE_MANAGER_TOKEN,
        useFactory: async (conf: CacheConfig) => {
          return cacheManager.caching({
            ...conf,
            store: conf.store === 'memory' ? conf.store : redisStore,
          });
        },
        inject: [(configKey || defaultCacheConfig.KEY)],
      },
    ];

    module.exports = [CACHE_MANAGER_TOKEN];

    return module;
  }

}
