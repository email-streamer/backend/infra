import { Inject } from '@nestjs/common';
import { Cache as CacheManager } from 'cache-manager';
import {
  CACHE_INJECTION_PROP,
  CACHE_KEY_PREFIX,
  CACHE_MANAGER_TOKEN,
} from './constants';
import { isCacheEnabled } from '../config/cache.config';

export interface CacheDecoratorOptions {
  key: string;
  ttl?: number;
  resetOnShutdown?: boolean;
}

export const Cache = (options: CacheDecoratorOptions): MethodDecorator => {
  if (!isCacheEnabled()) return () => { };

  const ttl = options.ttl || 60 * 5;

  return (
    target: Record<string, any>,
    methodKey: string | symbol,
    descriptor: PropertyDescriptor,
  ) => {
    const cacheManagerInjection = Inject(CACHE_MANAGER_TOKEN);
    const originMethod = descriptor.value;
    const cacheKeyBase = `${CACHE_KEY_PREFIX}${options.key}`;

    cacheManagerInjection(target, CACHE_INJECTION_PROP);

    descriptor.value = async function(...args: any[]) {
      const { [CACHE_INJECTION_PROP]: cache } = this as {
        [CACHE_INJECTION_PROP]: CacheManager;
      };

      const cacheKey = `${cacheKeyBase}[${args
        .map((res) => JSON.stringify(res))
        .join(',')}]`;

      try {
        const res = await cache.wrap(cacheKey, () => originMethod.call(this, ...args), { ttl });

        return res;
      } catch (err) {

        return originMethod.call(this, ...args);

      }
    };
  };
};
