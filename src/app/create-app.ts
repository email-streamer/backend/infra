import {
  Logger as BaseLogger,
  DynamicModule,
  INestApplication,
  Module,
} from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { RmqOptions, Transport } from '@nestjs/microservices';
import { SwaggerModule } from '@nestjs/swagger';
import { HealthCheckerController } from './health-checker';
import { CreateAppOptions } from './types';
import { CacheModule } from '../cache';
import {
  appConfig,
  ConfigModule,
  isRmqDisabled,
  rmqConfig,
} from '../config';
import { LoggerModule } from '../logger';
import { SYSTEM_LOGGER_TOKEN } from '../logger/constants';
import { genSwagger } from '../swagger';

@Module({})
class RootModule { }

async function connectRmq(app: INestApplication) {
  const appConf = app.get<ConfigType<typeof appConfig>>(appConfig.KEY);
  const rmqConf = app.get<ConfigType<typeof rmqConfig>>(rmqConfig.KEY);
  app.connectMicroservice<RmqOptions>({
    transport: Transport.RMQ,
    options: {
      urls: [rmqConf.url],
      queue: appConf.serviceName,
      queueOptions: {
        durable: true,
      },
    },
  });

  return app;
}

export async function createApp({ controllers = [], imports = [], providers = [], configs = [] }: CreateAppOptions): Promise<INestApplication> {
  const rootModule: DynamicModule = {
    module: RootModule,
    providers: [...providers],
    imports: [CacheModule.forRoot(), ConfigModule.forRoot(configs), LoggerModule.forRoot(), ...imports],
    controllers: [...controllers, HealthCheckerController],
  };

  const app = await NestFactory.create(rootModule, { bufferLogs: true });

  if (!isRmqDisabled) {
    await connectRmq(app);
  }

  const swagger = await genSwagger(app);
  SwaggerModule.setup('swagger', app, swagger);

  app.useLogger(app.get(SYSTEM_LOGGER_TOKEN));
  BaseLogger.flush();

  return app;
}
