import {
  Controller,
  Get,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';

@Controller()
export class HealthCheckerController {
  @Get('health')
  @HttpCode(HttpStatus.OK)
  async check() {
    return;
  }
}
