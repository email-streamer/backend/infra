import {
  DynamicModule,
  Global,
  Module,
} from '@nestjs/common';
import { SYSTEM_LOGGER_TOKEN } from './constants';
import { PinoNestLogger } from './logger.service';

@Global()
@Module({})
export class LoggerModule {

  static forRoot(): DynamicModule {

    return {
      module: LoggerModule,
      providers: [{
        provide: SYSTEM_LOGGER_TOKEN,
        useValue: new PinoNestLogger('system'),
      },
      ],
      exports: [SYSTEM_LOGGER_TOKEN],

    };
  }
}
