
import { registerAs } from '@nestjs/config';
import * as env from 'env-var';

export const rmqConfig = registerAs('rqm', () => ({
  url: env.get('RMQ_URL').asString(),
}));

export const isRmqDisabled = () => {
  return env.get('RMQ_URL').asString() ? false : true;
};
