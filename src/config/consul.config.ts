
import { registerAs } from '@nestjs/config';
import * as env from 'env-var';

export const consulConfig = registerAs('consul', () => ({
  host: env.get('CONSUL_HOST').default('localhost').asString(),
  port: env.get('CONSUL_PORT').default('8500').asString(),
}));
