import { registerAs } from '@nestjs/config';
import * as env from 'env-var';
import { CACHE_MEMORY_STORE } from '../cache/constants';

export const defaultCacheConfig = registerAs('cache', () => ({
  store: CACHE_MEMORY_STORE,
  ttl: env.get('CACHE_TTL')
    .example('10')
    .default('5')
    .asInt(),
}));

export const isCacheEnabled = () => {
  return !env.get('DISABLE_CACHE').asBool();
};
