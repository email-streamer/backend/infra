import { DynamicModule } from '@nestjs/common';
import { ConfigFactory, ConfigModule as NestConfigModule } from '@nestjs/config';
import { appConfig } from './app.config';
import { defaultCacheConfig } from './cache.config';
import { rmqConfig } from './rmq.config';

export class ConfigModule {
  static forRoot(extraConfigs: ConfigFactory[] = []): DynamicModule {
    const load: ConfigFactory[] = [
      defaultCacheConfig,
      appConfig,
      rmqConfig,
      ...extraConfigs,
    ];

    return {
      module: ConfigModule,
      imports: [NestConfigModule.forRoot({
        isGlobal: true,
        load,
      })],
    };
  }
}
