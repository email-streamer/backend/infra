
import { registerAs } from '@nestjs/config';
import * as env from 'env-var';

export const appConfig = registerAs('app', () => ({
  env: env.get('NODE_ENV').default('development').asString(),
  serviceName: env.get('SERVICE_NAME').example('MARKETING').default('NONAME').asString().toUpperCase(),
  httpPort: env.get('HTTP_PORT').default(3000).asInt(),
}));
