import { INestApplication } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import {
  DocumentBuilder,
  OpenAPIObject,
  SwaggerModule,
} from '@nestjs/swagger';
import { appConfig } from '../config';

export const genSwagger = async (app: INestApplication): Promise<OpenAPIObject> => {
  const config: ConfigType<typeof appConfig> = await app.get(appConfig.KEY);
  const docConfig = new DocumentBuilder();

  docConfig.setTitle(config.serviceName);

  const doc = SwaggerModule.createDocument(app, docConfig.build());

  return doc;
};
