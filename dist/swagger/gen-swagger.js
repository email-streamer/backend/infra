"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.genSwagger = void 0;
const swagger_1 = require("@nestjs/swagger");
const config_1 = require("../config");
const genSwagger = async (app) => {
    const config = await app.get(config_1.appConfig.KEY);
    const docConfig = new swagger_1.DocumentBuilder();
    docConfig.setTitle(config.serviceName);
    const doc = swagger_1.SwaggerModule.createDocument(app, docConfig.build());
    return doc;
};
exports.genSwagger = genSwagger;
//# sourceMappingURL=gen-swagger.js.map