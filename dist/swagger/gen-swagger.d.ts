import { INestApplication } from '@nestjs/common';
import { OpenAPIObject } from '@nestjs/swagger';
export declare const genSwagger: (app: INestApplication) => Promise<OpenAPIObject>;
