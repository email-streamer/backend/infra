"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CACHE_REDIS_STORE = exports.CACHE_MEMORY_STORE = exports.CACHE_INJECTION_PROP = exports.CACHE_KEY_PREFIX = exports.LOCAL_CACHE_INJECTION_TOKEN = exports.CACHE_MANAGER_TOKEN = void 0;
exports.CACHE_MANAGER_TOKEN = Symbol('MAIN_CACHE_MANAGER');
exports.LOCAL_CACHE_INJECTION_TOKEN = Symbol('LOCAL_CHACHE');
exports.CACHE_KEY_PREFIX = 'cache:methods:';
exports.CACHE_INJECTION_PROP = '$cache';
exports.CACHE_MEMORY_STORE = 'memory';
exports.CACHE_REDIS_STORE = 'redis';
//# sourceMappingURL=constants.js.map