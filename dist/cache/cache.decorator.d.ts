export interface CacheDecoratorOptions {
    key: string;
    ttl?: number;
    resetOnShutdown?: boolean;
}
export declare const Cache: (options: CacheDecoratorOptions) => MethodDecorator;
