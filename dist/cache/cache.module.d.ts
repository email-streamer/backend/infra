import { DynamicModule } from '@nestjs/common';
export declare class CacheModule {
    static forRoot(configKey?: string): DynamicModule;
}
