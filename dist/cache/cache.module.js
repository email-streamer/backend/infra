"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var CacheModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CacheModule = void 0;
const common_1 = require("@nestjs/common");
const cacheManager = require("cache-manager");
const cache_manager_ioredis_1 = require("cache-manager-ioredis");
const constants_1 = require("./constants");
const cache_config_1 = require("../config/cache.config");
let CacheModule = CacheModule_1 = class CacheModule {
    static forRoot(configKey) {
        const module = {
            module: CacheModule_1,
        };
        module.providers = [
            {
                provide: constants_1.CACHE_MANAGER_TOKEN,
                useFactory: async (conf) => {
                    return cacheManager.caching({
                        ...conf,
                        store: conf.store === 'memory' ? conf.store : cache_manager_ioredis_1.default,
                    });
                },
                inject: [(configKey || cache_config_1.defaultCacheConfig.KEY)],
            },
        ];
        module.exports = [constants_1.CACHE_MANAGER_TOKEN];
        return module;
    }
};
exports.CacheModule = CacheModule;
exports.CacheModule = CacheModule = CacheModule_1 = __decorate([
    (0, common_1.Module)({}),
    (0, common_1.Global)()
], CacheModule);
//# sourceMappingURL=cache.module.js.map