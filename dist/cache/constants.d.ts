export declare const CACHE_MANAGER_TOKEN: unique symbol;
export declare const LOCAL_CACHE_INJECTION_TOKEN: unique symbol;
export declare const CACHE_KEY_PREFIX = "cache:methods:";
export declare const CACHE_INJECTION_PROP = "$cache";
export declare const CACHE_MEMORY_STORE = "memory";
export declare const CACHE_REDIS_STORE = "redis";
