"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cache = void 0;
const common_1 = require("@nestjs/common");
const constants_1 = require("./constants");
const cache_config_1 = require("../config/cache.config");
const Cache = (options) => {
    if (!(0, cache_config_1.isCacheEnabled)())
        return () => { };
    const ttl = options.ttl || 60 * 5;
    return (target, methodKey, descriptor) => {
        const cacheManagerInjection = (0, common_1.Inject)(constants_1.CACHE_MANAGER_TOKEN);
        const originMethod = descriptor.value;
        const cacheKeyBase = `${constants_1.CACHE_KEY_PREFIX}${options.key}`;
        cacheManagerInjection(target, constants_1.CACHE_INJECTION_PROP);
        descriptor.value = async function (...args) {
            const { [constants_1.CACHE_INJECTION_PROP]: cache } = this;
            const cacheKey = `${cacheKeyBase}[${args
                .map((res) => JSON.stringify(res))
                .join(',')}]`;
            try {
                const res = await cache.wrap(cacheKey, () => originMethod.call(this, ...args), { ttl });
                return res;
            }
            catch (err) {
                return originMethod.call(this, ...args);
            }
        };
    };
};
exports.Cache = Cache;
//# sourceMappingURL=cache.decorator.js.map