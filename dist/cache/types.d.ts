import { CACHE_MEMORY_STORE, CACHE_REDIS_STORE } from './constants';
export interface InMemoryCacheConfig {
    store: typeof CACHE_MEMORY_STORE;
    ttl: number;
}
export interface RedisCacheConfig {
    store: typeof CACHE_REDIS_STORE;
    ttl: number;
    host?: string;
    port?: number;
    password?: string;
    db?: number;
}
export type CacheConfig = RedisCacheConfig | InMemoryCacheConfig;
