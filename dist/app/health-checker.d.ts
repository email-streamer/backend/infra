export declare class HealthCheckerController {
    check(): Promise<void>;
}
