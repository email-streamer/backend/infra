import { INestApplication } from '@nestjs/common';
import { CreateAppOptions } from './types';
export declare function createApp({ controllers, imports, providers, configs }: CreateAppOptions): Promise<INestApplication>;
