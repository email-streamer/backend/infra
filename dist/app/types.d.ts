import { Provider } from '@nestjs/common';
import { ConfigFactory } from '@nestjs/config';
export interface CreateAppOptions {
    imports?: any[];
    providers?: Provider[];
    controllers?: any[];
    configs?: ConfigFactory[];
}
