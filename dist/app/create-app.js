"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.createApp = void 0;
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const microservices_1 = require("@nestjs/microservices");
const swagger_1 = require("@nestjs/swagger");
const health_checker_1 = require("./health-checker");
const cache_1 = require("../cache");
const config_1 = require("../config");
const logger_1 = require("../logger");
const constants_1 = require("../logger/constants");
const swagger_2 = require("../swagger");
let RootModule = class RootModule {
};
RootModule = __decorate([
    (0, common_1.Module)({})
], RootModule);
async function connectRmq(app) {
    const appConf = app.get(config_1.appConfig.KEY);
    const rmqConf = app.get(config_1.rmqConfig.KEY);
    app.connectMicroservice({
        transport: microservices_1.Transport.RMQ,
        options: {
            urls: [rmqConf.url],
            queue: appConf.serviceName,
            queueOptions: {
                durable: true,
            },
        },
    });
    return app;
}
async function createApp({ controllers = [], imports = [], providers = [], configs = [] }) {
    const rootModule = {
        module: RootModule,
        providers: [...providers],
        imports: [cache_1.CacheModule.forRoot(), config_1.ConfigModule.forRoot(configs), logger_1.LoggerModule.forRoot(), ...imports],
        controllers: [...controllers, health_checker_1.HealthCheckerController],
    };
    const app = await core_1.NestFactory.create(rootModule, { bufferLogs: true });
    if (!config_1.isRmqDisabled) {
        await connectRmq(app);
    }
    const swagger = await (0, swagger_2.genSwagger)(app);
    swagger_1.SwaggerModule.setup('swagger', app, swagger);
    app.useLogger(app.get(constants_1.SYSTEM_LOGGER_TOKEN));
    common_1.Logger.flush();
    return app;
}
exports.createApp = createApp;
//# sourceMappingURL=create-app.js.map