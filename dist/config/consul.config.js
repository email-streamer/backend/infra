"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.consulConfig = void 0;
const config_1 = require("@nestjs/config");
const env = require("env-var");
exports.consulConfig = (0, config_1.registerAs)('consul', () => ({
    host: env.get('CONSUL_HOST').default('localhost').asString(),
    port: env.get('CONSUL_PORT').default('8500').asString(),
}));
//# sourceMappingURL=consul.config.js.map