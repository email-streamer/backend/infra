export declare const rmqConfig: (() => {
    url: string;
}) & import("@nestjs/config").ConfigFactoryKeyHost<{
    url: string;
}>;
export declare const isRmqDisabled: () => boolean;
