"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isRedisEnabled = exports.redisConfig = void 0;
const config_1 = require("@nestjs/config");
const env = require("env-var");
exports.redisConfig = (0, config_1.registerAs)('defaultRedis', () => ({
    host: env.get('REDIS_HOST')
        .asString(),
    port: env.get('REDIS_PORT')
        .asInt(),
    url: env.get('REDIS_URL')
        .asString(),
    username: env.get('REDIS_USERNAME')
        .asString(),
    password: env.get('REDIS_PASSWORD')
        .asString(),
    db: env.get('REDIS_DB')
        .asInt(),
    enableReadyCheck: true,
    collectRedisInfo: env.get('REDIS_INFO_METRICS_ENABLED')
        .default(0)
        .asBool(),
}));
const isRedisEnabled = () => {
    const redisUrl = env.get('REDIS_URL').asString();
    const redisHost = env.get('REDIS_HOST').asString();
    const redisPort = env.get('REDIS_PORT').asString();
    return redisUrl || (redisHost && redisPort);
};
exports.isRedisEnabled = isRedisEnabled;
//# sourceMappingURL=redis.config.js.map