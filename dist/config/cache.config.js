"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isCacheEnabled = exports.defaultCacheConfig = void 0;
const config_1 = require("@nestjs/config");
const env = require("env-var");
const constants_1 = require("../cache/constants");
exports.defaultCacheConfig = (0, config_1.registerAs)('cache', () => ({
    store: constants_1.CACHE_MEMORY_STORE,
    ttl: env.get('CACHE_TTL')
        .example('10')
        .default('5')
        .asInt(),
}));
const isCacheEnabled = () => {
    return !env.get('DISABLE_CACHE').asBool();
};
exports.isCacheEnabled = isCacheEnabled;
//# sourceMappingURL=cache.config.js.map