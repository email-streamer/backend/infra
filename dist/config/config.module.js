"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigModule = void 0;
const config_1 = require("@nestjs/config");
const app_config_1 = require("./app.config");
const cache_config_1 = require("./cache.config");
const rmq_config_1 = require("./rmq.config");
class ConfigModule {
    static forRoot(extraConfigs = []) {
        const load = [
            cache_config_1.defaultCacheConfig,
            app_config_1.appConfig,
            rmq_config_1.rmqConfig,
            ...extraConfigs,
        ];
        return {
            module: ConfigModule,
            imports: [config_1.ConfigModule.forRoot({
                    isGlobal: true,
                    load,
                })],
        };
    }
}
exports.ConfigModule = ConfigModule;
//# sourceMappingURL=config.module.js.map