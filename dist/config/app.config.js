"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.appConfig = void 0;
const config_1 = require("@nestjs/config");
const env = require("env-var");
exports.appConfig = (0, config_1.registerAs)('app', () => ({
    env: env.get('NODE_ENV').default('development').asString(),
    serviceName: env.get('SERVICE_NAME').example('MARKETING').default('NONAME').asString().toUpperCase(),
    httpPort: env.get('HTTP_PORT').default(3000).asInt(),
}));
//# sourceMappingURL=app.config.js.map