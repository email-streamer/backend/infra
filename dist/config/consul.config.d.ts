export declare const consulConfig: (() => {
    host: string;
    port: string;
}) & import("@nestjs/config").ConfigFactoryKeyHost<{
    host: string;
    port: string;
}>;
