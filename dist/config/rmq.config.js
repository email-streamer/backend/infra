"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isRmqDisabled = exports.rmqConfig = void 0;
const config_1 = require("@nestjs/config");
const env = require("env-var");
exports.rmqConfig = (0, config_1.registerAs)('rqm', () => ({
    url: env.get('RMQ_URL').asString(),
}));
const isRmqDisabled = () => {
    return env.get('RMQ_URL').asString() ? false : true;
};
exports.isRmqDisabled = isRmqDisabled;
//# sourceMappingURL=rmq.config.js.map