export declare const redisConfig: (() => {
    host: string;
    port: number;
    url: string;
    username: string;
    password: string;
    db: number;
    enableReadyCheck: boolean;
    collectRedisInfo: boolean;
}) & import("@nestjs/config").ConfigFactoryKeyHost<{
    host: string;
    port: number;
    url: string;
    username: string;
    password: string;
    db: number;
    enableReadyCheck: boolean;
    collectRedisInfo: boolean;
}>;
export declare const isRedisEnabled: () => string;
