import { DynamicModule } from '@nestjs/common';
import { ConfigFactory } from '@nestjs/config';
export declare class ConfigModule {
    static forRoot(extraConfigs?: ConfigFactory[]): DynamicModule;
}
