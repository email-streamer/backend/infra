export declare const defaultCacheConfig: (() => {
    store: string;
    ttl: number;
}) & import("@nestjs/config").ConfigFactoryKeyHost<{
    store: string;
    ttl: number;
}>;
export declare const isCacheEnabled: () => boolean;
