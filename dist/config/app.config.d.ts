export declare const appConfig: (() => {
    env: string;
    serviceName: string;
    httpPort: number;
}) & import("@nestjs/config").ConfigFactoryKeyHost<{
    env: string;
    serviceName: string;
    httpPort: number;
}>;
