import { LoggerService } from '@nestjs/common';
export declare class PinoNestLogger implements LoggerService {
    private readonly contextName;
    constructor(contextName: string);
    private logger;
    verbose(message: any, ...optionalParams: any[]): void;
    debug(message: any, ...optionalParams: any[]): void;
    log(message: any, ...optionalParams: any[]): void;
    warn(message: any, ...optionalParams: any[]): void;
    error(message: any, ...optionalParams: any[]): void;
    private call;
    private isWrongExceptionsHandlerContract;
}
