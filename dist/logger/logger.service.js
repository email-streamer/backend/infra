"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PinoNestLogger = void 0;
const common_1 = require("@nestjs/common");
const pino_1 = require("pino");
const pinoLogger = (0, pino_1.default)();
let PinoNestLogger = class PinoNestLogger {
    constructor(contextName) {
        this.contextName = contextName;
        this.logger = pinoLogger.child({});
    }
    verbose(message, ...optionalParams) {
        this.call('trace', message, ...optionalParams);
    }
    debug(message, ...optionalParams) {
        this.call('debug', message, ...optionalParams);
    }
    log(message, ...optionalParams) {
        this.call('info', message, ...optionalParams);
    }
    warn(message, ...optionalParams) {
        this.call('warn', message, ...optionalParams);
    }
    error(message, ...optionalParams) {
        this.call('error', message, ...optionalParams);
    }
    call(level, message, ...optionalParams) {
        const objArg = {};
        let params = [];
        if (optionalParams.length !== 0) {
            objArg[this.contextName] = optionalParams[optionalParams.length - 1];
            params = optionalParams.slice(0, -1);
        }
        if (typeof message === 'object') {
            if (message instanceof Error) {
                objArg.err = message;
            }
            else {
                Object.assign(objArg, message);
            }
            this.logger[level](objArg, ...params);
        }
        else if (this.isWrongExceptionsHandlerContract(level, message, params)) {
            objArg.err = new Error(message);
            objArg.err.stack = params[0];
            this.logger[level](objArg);
        }
        else {
            this.logger[level](objArg, message, ...params);
        }
    }
    isWrongExceptionsHandlerContract(level, message, params) {
        return (level === 'error' &&
            typeof message === 'string' &&
            params.length === 1 &&
            typeof params[0] === 'string' &&
            /\n\s*at /.test(params[0]));
    }
};
exports.PinoNestLogger = PinoNestLogger;
exports.PinoNestLogger = PinoNestLogger = __decorate([
    (0, common_1.Injectable)({ scope: common_1.Scope.TRANSIENT }),
    __metadata("design:paramtypes", [String])
], PinoNestLogger);
//# sourceMappingURL=logger.service.js.map