module.exports = {
  extends: ['./node_modules/@infra/eslint'],
  parserOptions: {
    project: './tsconfig.eslint.json'
  }
}
